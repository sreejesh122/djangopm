from django.core.management.base import BaseCommand, CommandError
from pm.models import Project, Commits, User, Issues
from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.shortcuts import redirect
import requests
from datetime import datetime
from django.http import HttpResponse
import datetime
from datetime import datetime, timedelta

class Command(BaseCommand):
	help = 'Type the help text here'

	def handle(self, *args, **options):

		users = User.objects.all()
		for user in users:
			user.save()
			url = 'https://gitlab.com/api/v4/users/'+user.username+'/projects'
			headers = {'Authorization': 'Bearer '+user.access_token}
			r = requests.get(url, headers=headers)
			projects = r.json()
			project_list = []
			open_issues = 0
			commits_no1 = 0
			for project in projects:
				project_list.append(project['id'])
				project_obj = Project.objects.filter(project_id=project['id'])
				if len(project_obj) == 0:
					project_obj = Project(project_id=project['id'], name=project['name'], description=project['description'], created_at=project['created_at'])
					project_obj.save()
				else:
					project_obj = Project.objects.get(project_id=project['id'])
					issue_url = 'https://gitlab.com/api/v4/projects/'+str(project_obj.project_id)+'/issues?state=opened'
					issues = requests.get(issue_url, headers=headers)
					issues = issues.json()

					commit_url = 'https://gitlab.com/api/v4/projects/'+str(project_obj.project_id)+'/repository/commits'
					commits = requests.get(commit_url, headers=headers)
					commits = commits.json()
					for commit in commits:
						commit_obj = Commits.objects.filter(commit_id=commit['id'])
						if len(commit_obj) == 0:
							commit_obj = Commits(commit_id=commit['id'], name=commit['title'], created_at=commit['created_at'], message=commit['message'], project_id=project_obj)
							commit_obj.save()

					commit_url = 'https://gitlab.com/api/v4/projects/'+str(project_obj.project_id)+'/repository/commits'
					issue_url = 'https://gitlab.com/api/v4/projects/'+str(project_obj.project_id)+'/issues'
					r = requests.get(issue_url, headers=headers)
					issues = r.json()
					for issue in issues:
						issue_obj = Issues.objects.filter(issue_id=issue.get('id'))
						if len(issue_obj) == 0:
							issue_obj = Issues(issue_id=issue.get('id'), created_at=issue.get('created_at'), project_id=project_obj)
							issue_obj.save()
						else:
							issue_obj = Issues.objects.get(issue_id=issue.get('id'))
							issue_obj.name = issue.get('title')
							issue_obj.description = issue.get('description')
							issue_obj.state = issue.get('state')
							for assignee in issue.get('assignees'):
								assignee_obj = User.objects.get(project_user_id=assignee['id'])

								if issue_obj.assignees.filter(id=assignee_obj.id).exists():
									pass
								else:
									issue_obj.assignees.add(assignee_obj.id)

							issue_obj.save()
					if project_obj.users.filter(id=user.id).exists():
						pass
					else:
						project_obj.users.add(user.id)
					project_obj.save()

