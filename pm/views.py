from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from . models import Project, Commits, User, Issues
from django.shortcuts import redirect
import requests
from datetime import datetime
from django.http import HttpResponse
import datetime
from datetime import datetime, timedelta


def user_login(request):
	if request.method == 'POST':
		username = request.POST.get('username')
		password = request.POST.get('password')
		email = request.POST.get('email')
		# print(username,password)
		url = 'https://gitlab.com/oauth/token'
		data = {
				"grant_type"    : "password",
				"username"      : username,
				"password"      : password
				}
		user_auth = requests.post(url, data = data)
		user_auth = user_auth.json()

		print(user_auth)
		if not 'error' in user_auth:

			user_obj = User(username=username, access_token=user_auth['access_token'], email=email)
			user_obj.save()
			user_obj.set_password(password)
			user_obj.save()
			
			url = 'https://gitlab.com/api/v4/users'
			data = {
				"username"      : username,
				}
			r = requests.get(url, data = data)
			user_details = r.json()
			print(user_details)
			# print(sdff)
			user_obj.project_user_id = user_details[0]['id']
			user_obj.first_name = user_details[0]['name']
			user_obj.save()

			return redirect('accounts/login/')
		else:
			print("Someone tried to login and failed.")
			print("They used username: {} and password: {}".format(username,password))
			return HttpResponse("Invalid login details given")
	else:
		return render(request, 'pm/user_login.html', {})


def dashboard(request):
	current_user = request.user
	user = User.objects.get(user_ptr_id=current_user.id)
	# projects = Project.objects.all()
	projects = Project.objects.filter(users__in=[user.id])

	open_issues = 0
	labbeled_issues = 0
	commits = 0
	today = datetime.today().date()
	yestrerday = today + timedelta(days=-1)
	for project in projects:
		open_issues += Issues.objects.filter(project_id=project.id, state__contains='opened').count()
		commits += Commits.objects.filter(project_id=project.id, created_at__date=yestrerday).count()
		labbeled_issues += Issues.objects.filter(project_id=project.id, labels__contains='Doing').count()
	return render(request, 'pm/project_dashboard.html', {'projects': projects, 'open_issues': open_issues, 'commits':commits, 'labbeled_issues': labbeled_issues})



def refresh(request):
	if request.method == 'GET':
		current_user = request.user
		user = User.objects.get(user_ptr_id=current_user.id)
		url = 'https://gitlab.com/api/v4/users/'+user.username+'/projects'
		headers = {'Authorization': 'Bearer '+user.access_token}
		r = requests.get(url, headers=headers)
		projects = r.json()
		project_list = []
		open_issues = 0
		commits_no1 = 0
		for project in projects:
			project_list.append(project['id'])
			project_obj = Project.objects.filter(project_id=project['id'])
			if len(project_obj) == 0:
				project_obj = Project(project_id=project['id'], name=project['name'], description=project['description'], created_at=project['created_at'])
				project_obj.save()
			else:
				project_obj = Project.objects.get(project_id=project['id'])
				issue_url = 'https://gitlab.com/api/v4/projects/'+str(project_obj.project_id)+'/issues?state=opened'
				issues = requests.get(issue_url, headers=headers)
				issues = issues.json()

				commit_url = 'https://gitlab.com/api/v4/projects/'+str(project_obj.project_id)+'/repository/commits'
				commits = requests.get(commit_url, headers=headers)
				commits = commits.json()
				for commit in commits:
					print(commit['id'])
					commit_obj = Commits.objects.filter(commit_id=commit['id'])
					if len(commit_obj) == 0:
						commit_obj = Commits(commit_id=commit['id'], name=commit['title'], created_at=commit['created_at'], message=commit['message'], project_id=project_obj)
						commit_obj.save()

				# url = 'https://gitlab.com/api/v4/users/'+username+'/projects'
				commit_url = 'https://gitlab.com/api/v4/projects/'+str(project_obj.project_id)+'/repository/commits'
				issue_url = 'https://gitlab.com/api/v4/projects/'+str(project_obj.project_id)+'/issues'
				r = requests.get(issue_url, headers=headers)
				issues = r.json()
				for issue in issues:
					issue_obj = Issues.objects.filter(issue_id=issue.get('id'))
					if len(issue_obj) == 0:
						issue_obj = Issues(issue_id=issue.get('id'), created_at=issue.get('created_at'), project_id=project_obj)
						issue_obj.save()
					else:
						issue_obj = Issues.objects.get(issue_id=issue.get('id'))
						issue_obj.name = issue.get('title')
						issue_obj.description = issue.get('description')
						issue_obj.state = issue.get('state')
						label = ''
						for labels in issue.get('labels'):
							label = label +' ' +labels
						issue_obj.labels = label
						for assignee in issue.get('assignees'):
							assignee_obj = User.objects.get(project_user_id=assignee['id'])

							if issue_obj.assignees.filter(id=assignee_obj.id).exists():
								pass
							else:
								issue_obj.assignees.add(assignee_obj.id)

						issue_obj.save()
					print (issue_obj.state)
				if project_obj.users.filter(id=user.id).exists():
					pass
				else:
					project_obj.users.add(user.id)
				project_obj.save()
		projects = Project.objects.filter(users__in=[user.id])

		open_issues = 0
		labbeled_issues = 0
		commits_nos = 0
		today = datetime.today().date()
		yestrerday = today + timedelta(days=-1)
		for project in projects:
			open_issues += Issues.objects.filter(project_id=project.id, state__contains='opened').count()
			commits_nos += Commits.objects.filter(project_id=project.id, created_at__date=yestrerday).count()
			labbeled_issues += Issues.objects.filter(project_id=project.id, labels__contains='Doing').count()
		return render(request, 'pm/project_dashboard.html', {'projects': projects, 'open_issues': open_issues, 'commits':commits_nos, 'labbeled_issues': labbeled_issues})

def project_detail(request, pk):
	current_user = request.user
	user = User.objects.get(user_ptr_id=current_user.id)
	project = Project.objects.get(id=pk)
	today = datetime.today().date()
	yestrerday = today + timedelta(days=-1)
	open_issues = Issues.objects.filter(project_id=pk, state__contains='opened').count()
	labbeled_issues = Issues.objects.filter(project_id=pk, labels__contains='Doing').count()
	commits = Commits.objects.filter(project_id=pk, created_at__date=yestrerday)

	return render(request, 'pm/project_details.html', {'project': project, 'commits':commits, 'open_issues':open_issues, 'labbeled_issues':labbeled_issues})


