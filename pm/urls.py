from django.urls import path
from . import views
from django.views.generic import RedirectView



urlpatterns = [
    path('login', views.user_login, name='user_login'),
    # path('', views.user_login, name='user_login'),
    path('dashboard', views.dashboard, name='project_dashboard'),
    path('refresh', views.refresh, name='project_refresh'),
    path('project/<int:pk>/', views.project_detail, name='project_detail'),
    path('accounts/login', RedirectView.as_view(url='')),

]