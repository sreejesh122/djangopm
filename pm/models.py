from django.conf import settings
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User as Auth_User


class User(Auth_User):

    project_user_id = models.IntegerField(default=0)
    access_token = models.CharField(max_length=200, null=True)
    test = models.IntegerField(default=1)


class Project(models.Model):
    
    project_id = models.IntegerField(null=True)
    name = models.CharField(max_length=200, null=True)
    description = models.TextField()
    created_at = models.DateTimeField(null=True)
    users = models.ManyToManyField(User, verbose_name="Project Members")

    def __str__(self):
        return self.name


class Commits(models.Model):

    name = models.CharField(max_length=200, null=True)
    project_id = models.ForeignKey(Project, on_delete=models.CASCADE, default=1)
    created_at = models.DateTimeField(null=True)
    commit_id = models.CharField(max_length=200, null=True)
    message = models.CharField(max_length=200, null=True)


class Issues(models.Model):

    issue_id = models.IntegerField(null=True)
    name = models.CharField(max_length=200, null=True)
    project_id = models.ForeignKey(Project, on_delete=models.CASCADE, default=1)
    created_at = models.DateTimeField(null=True)
    description = models.CharField(max_length=200, null=True)
    state = models.CharField(max_length=50, null=True)
    assignees = models.ManyToManyField(User, verbose_name="Project Members")
    labels = models.TextField()
    
