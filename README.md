# djangopm

Mail Configuration
djangopm/project/setting.py

change the below details
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'user@gmail.com'
EMAIL_HOST_PASSWORD = 'passsword'
EMAIL_PORT = 587

Add Cron entry for Scheduled Action

In Linux - Terminal Command

crontab -e

Add the below line for fetching on a daily basis (at midnight), and storing in the local database.

0 0 * * * /path/to/virtualenv/bin/python /path/to/project/manage.py mytask

Add the below code for sending a mail to the PM if there any issues/tickets labeled as doing and no updates made in the last 24 hours.

0 * * * * /path/to/virtualenv/bin/python /path/to/project/manage.py sendmail


After installing and Configuration Goto

localhost:port/accounts/login
